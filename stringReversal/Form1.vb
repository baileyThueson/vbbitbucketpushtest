﻿Public Class Form1
    Private Sub btnReverse_Click(sender As Object, e As EventArgs) Handles btnReverse.Click
        Try
            Dim strInput As String = txtInput.Text.Trim 'Get Input
            rtbResults.AppendText(stringReverse(strInput)) 'Reverse And Output
        Catch ex As Exception
            MsgBox("ERROR: " & System.Environment.NewLine & ErrorToString(), MessageBoxIcon.Error, "ERROR") 'Exception Handling
        End Try
    End Sub
    'To Reverse A String, Copy-Paste The Function, and Pass the String To Be Reversed.
    Private Function stringReverse(ByVal strPass) As String
        'Declarations
        Dim i As Integer
        Dim strBuild As String
        Dim blnStringHasChar As Boolean = True
        'Reverse Input
        Dim intIndex = strPass.Trim.IndexOf(" ") 'Getting Index Of First Delimmiter
        i = intIndex 'Setting i to Substring Length
        Do While blnStringHasChar 'While String Has Characters
            If intIndex <> -1 Then 'If The Index is -1, No Delimmiter Is Found
                For i = i - 1 To 0 Step -1 'Loop For The Substring
                    strBuild &= strPass.Substring(i, 1) 'Append Chars In Reverse Order From Substring
                Next
                strBuild &= " " 'Append a Space For Neatness
                strPass = strPass.Remove(0, intIndex).Trim 'Remove Used Substring To Find Next Word
                'This Destructive Algorithm Will Be Changed To Include A Delimmiter Set, Check For All Delimmiters In The Set,
                'May Be Made Non-Destructive.
                intIndex = strPass.IndexOf(" ") 'Find Next Delimmiter Index
                i = intIndex 'Set i to Delim. Index
            Else 'If No Delim. Exist, Reverse Entire (Or Remainder Of) String
                For i = strPass.Length - 1 To 0 Step -1 'From the End Of the String To the Beginning
                    strBuild &= strPass.Substring(i, 1) 'Append Characters
                Next
                blnStringHasChar = False 'End Loop
            End If
        Loop
        Return "++" & strBuild.Trim & "++" & System.Environment.NewLine 'Trim and Return
    End Function
End Class
